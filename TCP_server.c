/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 10/20/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: TCP_server.c
 * Implements HTTP server 
 * usage: ./server <port>
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <sys/wait.h>
#include <signal.h>

#define BUFSIZE (4000)
#define MAX_COUNT (1000)

/****************************************************************************************************
*
* Global Variable
*
****************************************************************************************************/
char www_root[100] = "/home/kiranhegde/Downloads/PA2/www";
int sockfd;

void signal_handler()
{
	close(sockfd);
	printf("\nSignal Handler\n");
	exit(0);
}

/****************************************************************************************************
*
* @name fork_function
* @brief Handles the data sending in child process 
*
* After a request has been made by the client, the server creates a child process to handle it and 
* calls this function. This handles all data transfer  
*
* @param int client_sock client socket 
*
* @return 0 on No error
* @return 1 on error
*
****************************************************************************************************/
unsigned int fork_function(int client_sock)
{
	FILE *file_ptr;
	int  n, file_size, read_size;
	char buf[BUFSIZE], type[25];
	char *ptr, *post, *str, request[4000], post_str[BUFSIZE], *version;
	char header[4000];

	/* receive the data from client */
	if(recv(client_sock, buf, BUFSIZE, 0) < 0)
	{
		printf("[ERROR] Cound Not Receive from Client\n");
		return 1;
	}
	memcpy(post_str, buf, BUFSIZE);
	str = strtok(buf, " ");
	strcpy(request, str);
	str = strtok(NULL, " ");

	/* check the version */
	version = strtok(NULL, "\r");
	if((strcmp(version, "HTTP/1.1")!=0) && (strcmp(version, "HTTP/1.0")!=0))
	{
		/* if bad version, send back the error */
		if(strcmp(request, "GET") == 0)
			sprintf(header, "%s 500 BadVersion\r\nContent-type: text/html\n\n<html><body>Bad Request: Invalid Version</body></html>", version);
		else if(strcmp(request, "POST") == 0)
		{
			if((post = strstr(post_str, "\r\n\r\n")))
			{
				strcpy(post_str, post+4);
				sprintf(header, "%s 500 Bad Version OK\r\nContent-Type: text/html\r\n<html><body><pre><h1>%s</h1></pre>", version, post_str);
			}
		}		
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] Bad Version %s\n", version);
		return 1;
	}

	/* check for homepage condition. i.e. if no path is mentioned */
	if((strcmp(str, "/")) == 0 || strcmp(str, "/NULL") == 0) 
	{
		strcat(www_root, "/index.html");
		strcpy(type, "text/html");
	}
	else
	{
		strcat(www_root, str);
		/* check if format is mentioned, if not throw an error */
		if(!(ptr = strchr(str, '.')))
		{
			if(strcmp(request, "GET") == 0)
				sprintf(header, "%s 404 Not Found\r\nContent-type: text/html\n\n<html><body>404 Not found. URL does not exist</body></html>", version);	
			else if(strcmp(request, "POST") == 0)
			{
				if((post = strstr(post_str, "\r\n\r\n")))
				{
					strcpy(post_str, post+4);
					sprintf(header, "%s 404 Not Found\r\nContent-type: text/html\n\n<html><body><pre><h1>%s</h1></pre>", version, post_str);
				}
			}
			send(client_sock, header, strlen(header), 0);
			printf("[ERROR] File Not Found %s\n", str);
			return 1;
		}
		ptr++;
		
		/* check which type of document has to be sent i.e. Content-Type*/
		if (!(strcmp(ptr, "gif")))
			strcpy(type, "image/gif");
		else if (!(strcmp(ptr, "jpg") || strcmp(ptr, "jpeg")))
	       	        strcpy(type, "image/jpg");
		else if (!(strcmp(ptr, "png")))
	             	strcpy(type, "image/png");
		else if (!(strcmp(ptr, "txt")))
	              	strcpy(type, "text/plain");
		else if (!(strcmp(ptr, "css")))
	              	strcpy(type, "text/css");
		else if (!(strcmp(ptr, "html")))
	                strcpy(type, "text/html");
		else if (!(strcmp(ptr, "js")))
	                strcpy(type, "application/javascript");
		else if (!(strcmp(ptr, "json")))
	                strcpy(type, "application/json");
		else if (!(strcmp(ptr, "ico")))
	                 strcpy(type, "image/x-icon");
	}

	/* open the file, if not found send the error code */
	if(!(file_ptr = fopen(www_root, "r")))
	{
		if(strcmp(request, "GET") == 0)
			sprintf(header, "%s 404 Not Found\r\nContent-type: text/html\n\n<html><body>404 Not found. URL does not exist</body></html>", version);	
		else if(strcmp(request, "POST") == 0)
		{
			if((post = strstr(post_str, "\r\n\r\n")))
			{
				strcpy(post_str, post+4);
				sprintf(header, "%s 404 Not Found\r\nContent-type: text/html\n\n<html><body><pre><h1>%s</h1></pre>", version, post_str);
			}
		}
		send(client_sock, header, strlen(header), 0);
		printf("[ERROR] File Not Found %s\n", str);
		return 1;
	}

	/* find the size of the file for Content-Length */
	fseek(file_ptr, 0L, SEEK_END);
	file_size = ftell(file_ptr);
	rewind(file_ptr);

	/* Adjust the header to satisfy GET or POST request */
	if(strcmp(request, "GET") == 0)
		sprintf(header, "%s 200 OK\r\nContent-Type: %s\r\nContent-Length: %u\r\n\r\n", version, type, file_size);
	else if(strcmp(request, "POST") == 0)
	{
		if((post = strstr(post_str, "\r\n\r\n")))
		{
			strcpy(post_str, post+4);
			sprintf(header, "%s 200 OK\r\nContent-Type: %s\r\nContent-Length: %u\r\n\r\n<html><body><pre><h1>%s</h1></pre>", version, type, file_size, post_str);
			printf("\n%s\n", header);
		}
	}

	/* send the header first */	
	send(client_sock, header, strlen(header), 0);
	bzero(buf, BUFSIZE);

	/* send whole file */
	while((read_size = fread(buf, 1, BUFSIZE, file_ptr))>0)
	{
		send(client_sock, buf, read_size, 0);
		bzero(buf, BUFSIZE);
	}
	fclose(file_ptr);
	printf("[SUCCESS] Content Sent\n");
	
	/* return 0 on no error*/
	return 0;
}


/****************************************************************************************************
*
* @name Main
* @brief Implements the server functionality 
*
* This is the main function where all the server functionalities are implemented. 
*
* @param argc - argument count 
* @param **argv - pointer to argument  
*
* @return 0 on No error
*
****************************************************************************************************/
int main(int argc, char **argv) 
{
	int client_sock; /* socket */
	int portno; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int ret, count = 0; /* message byte size */
	int pid;

	/* 
	* check command line arguments 
	*/
	if (argc != 2) 
	{
    		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(1);
	}
	portno = atoi(argv[1]);

	/* 
	* socket: create the parent socket 
	*/
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		printf("ERROR opening socket");
	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt));
	/*
	* build the server's Internet address
	*/
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);

	/* 
	* bind: associate the parent socket with a port 
	*/
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
		printf("ERROR on binding");

	/* 
	* main loop: wait for a datagram, then echo it
	*/
	listen(sockfd, 1000);
	clientlen = sizeof(serveraddr);
	signal(SIGINT, signal_handler);
	while(1)
	{
		if((client_sock = accept(sockfd, (struct sockaddr *)&serveraddr, (socklen_t *)&clientlen)) < 0)
		{
			printf("Accept failed\n");
			return 1;
		}
		pid = fork();
		if (pid<0) return -1;
		else if (pid == 0)
		{
			close(sockfd);
			ret = fork_function(client_sock);
		        shutdown(client_sock, SHUT_RDWR);
			close(client_sock);
			exit(0);	
		}
		else
		{
			close(client_sock);
			waitpid(0, NULL, WNOHANG);
		}
	}
	return 0;
}
