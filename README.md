﻿Date: 10/21/2018
Programming Assignment 2 (CSCI 5273) - HTTP Server Programming
Submitted by Kiran Narayana Hegde


I have implemented a HTTP server which supports multiple client connection. I have used fork() to create child process and handled data transfer there.


It supports version HTTP/1.1 and HTTP/1.0.  “GET” and “POST” requests are supported by the server. Index.html is taken as homepage. 


POST and GET methods can be verified using developer console of browser. 




How to build and use the application ?


1. There is a directory named ‘HTTP_Server’ in the submitted .tar file. Untar it
2. Go to directory and use the following make command to build client application
$ make
1. In one terminal, run the server application using the following command
$ ./server <port_no>
Note: Port Number should be greater than 5000
1. Open browser and go to “localhost:<port_no>”