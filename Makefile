CC=gcc

.PHONY: all
all:
	$(CC) TCP_server.c -o server

.PHONY: clean
clean:
	rm -f server
